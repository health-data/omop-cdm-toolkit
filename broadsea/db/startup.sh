#!/bin/bash

# Wait for PostgreSQL to be ready
until pg_isready -p 5432 -U $POSTGRES_USER; do
  echo "Waiting for PostgreSQL to start..."
  sleep 2
done

# Execute the schema and data load commands
psql -U "$POSTGRES_USER" -d "$POSTGRES_DB"  -c "ALTER SYSTEM SET max_wal_size = '1GB'; SELECT pg_reload_conf();"
psql -U "$POSTGRES_USER" -d "$POSTGRES_DB" -f /tmp/omop_cdm_schema.sql
pg_restore --disable-triggers -U "$POSTGRES_USER" -d "$POSTGRES_DB" -F c /tmp/hospital_edge.dump