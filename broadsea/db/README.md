pg_dump -U <username> -d <database_name> -f data_dump.sql

docker build -t your-dockerhub-username/postgres-sample .

docker build -t alabarga/bsc-atlasdb \
--build-arg POSTGRES_DB=hospital_edge \
--build-arg POSTGRES_USER=postgres \
--build-arg POSTGRES_PASSWORD=pg_rulez .

docker login

docker push your-dockerhub-username/postgres-sample

docker run -d -p 5432:5432 --name postgres-container your-dockerhub-username/postgres-sample

docker run -d --name postgres-container \
-p 5432:5432 \
--volume=/mnt/local_data/test_postgres:/var/lib/postgresql/data \
--env POSTGRES_DB=hospital_edge \
--env POSTGRES_USER=postgres \
--env POSTGRES_PASSWORD=pg_rulez \
--shm-size=1Gb \
alabarga/bsc-atlasdb


docker run --env=POSTGRES_PASSWORD=pg_rulez --env=POSTGRES_USER=postgres --volume=/mnt/local_data/postgres/data:/var/lib/postgresql/data --volume=/home/alabarga/BSC/code/omop-cdm-toolkit/broadsea/db/:/backup -p 5432:5432 --shm-size=1Gb -d postgres

psql -U postgres -h localhost -p 5433 -c "CREATE DATABASE hospital_edge;"

psql -U postgres -h localhost -p 5433 -d hospital_edge -f /home/alabarga/BSC/code/omop-cdm-toolkit/broadsea/db/omop_cdm_schema.sql

pg_restore --disable-triggers -U postgres -h localhost -p 5433 -d hospital_edge -F c /home/alabarga/BSC/code/omop-cdm-toolkit/broadsea/db/hospital_edge.dump
