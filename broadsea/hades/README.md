# Changes
We changed the docker-compose file of the [Broadsea](https://github.com/OHDSI/Broadsea) repository. More specific, we changed the following containers: 
- broadsea-ares - using the official [Ares](https://github.com/OHDSI/Ares) repository. 
- broadsea-hades  updating the image version using the official [Hades]() respository.

    First, we build the image: 
        
    1. Create a Personal access tokens from Github (Settings > Developer Settings > Personal access tokens (classic)).
        
    2. Create a file called 'GITHUB_PAT' in the localhost with the Personal access token. 
        
    3. Build the image using the following commad: 
       ```
        $ docker build --secret_id=build_github_pat,src=_GITHUB_PAT_root_ . -t ohdsi-hades
       ```
    
    Then, we pull the docker image in the DockerHub: 
        
    1. Create a repository in the DockerHub:
       ```
       repository_name = ohdsi-hades
       ```
    
    2. Etiquetar la imagen creada con el nombbre del repositorio.
       ```
       $ sudo docker tag ohdsi-hades:latest judmartinez/ohdsi-hades:latest
       ```
       > _$ docker tag nombre-de-tu-imagen:etiqueta nombre-de-usuario/nombre-del-repositorio:etiqueta_
    3. Iniciar sesión
       ```
       $ sudo docker login
       ```
    4. Subir la imagen etiquetada al nuevo repositorio
       ```
       $ sudo docker push judmartinez/ohdsi-hades:latest
       ```

Also, we created a new volume called *ares-data* in order to be able to share data between HADES & ARES.

> NOTE: 
> Check *ARES-generation.R* to provide content to the ARES interface. 

# Useful Commands
```
$ docker compose pull && docker-compose --profile default --profile ares up -d
```

Or: 

```
$docker-compose --profile default --profile ares up -d
```


To shutdown: 
```
$ docker-compose down
```

