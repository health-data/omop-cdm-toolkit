# Quick start
1. Download and install Docker. 
2. Clone this repository. 
3. Open a terminal window and navigate to the directory where this INSTALL.md file is located.
4. Start the Broadsea Docker Container using the following command: 
   ```
   docker compose pull && docker-compose --profile default --profile ares up -d
   ```
5. In the web browser open the URL: ```http://127.0.0.1 ```.
   - Atlas 
   - Ares (to use it you must the script *ARES-generation.R* in HADES)
   - Hades
        - user: ohdsi
        - password: mypass


## Improvements
- Update the database 
- That the same R script renames the *dqdResultsFile* 